package io.polybius.phonevalidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MobilePhoneNumberValidator {

    private final static Map<Integer, String> DIGIT_2_ISO = new HashMap<>();

    static {
        DIGIT_2_ISO.put(370, "LT");
        DIGIT_2_ISO.put(371, "LV");
        DIGIT_2_ISO.put(372, "EE");
        DIGIT_2_ISO.put(32, "BE");
    }

    private final static Map<Integer, Set<String>> RULE_START_PER_DIGIT = new HashMap<>(); // Phone should start from either of these values

    static {
        RULE_START_PER_DIGIT.put(370, Set.of("6"));
        RULE_START_PER_DIGIT.put(371, Set.of("2"));
        RULE_START_PER_DIGIT.put(372, Set.of("5"));
        RULE_START_PER_DIGIT.put(32, Set.of("456", "47", "48", "49"));
    }

    private final static Map<Integer, Set<Integer>> RULE_LENGTH_PER_DIGIT = new HashMap<>(); // Phone length should be one of values

    static {
        RULE_LENGTH_PER_DIGIT.put(370, Set.of(8));
        RULE_LENGTH_PER_DIGIT.put(371, Set.of(8));
        RULE_LENGTH_PER_DIGIT.put(372, Set.of(7,8));
        RULE_LENGTH_PER_DIGIT.put(32, Set.of(9));
    }

    private ValidationResultDto result = null;

    public static void main(String[] args) {
        // Sample args: +324(561)23456 324561234561 37061234567 370612345671 37161234567 37121234567

        if (args.length > 0) {
            MobilePhoneNumberValidator mobilePhoneNumberValidator = new MobilePhoneNumberValidator();
            mobilePhoneNumberValidator.validate(List.of(args));
            mobilePhoneNumberValidator.printResults();
        }
    }

    public ValidationResultDto validate(List<String> phoneNumbers) {
        result = new ValidationResultDto();
        result.invalidPhones = new ArrayList<>();
        result.validPhonesByCountry = new HashMap<>();

        for (String phoneNumber : phoneNumbers) {
            String phoneNumberModified = replaceSpareChars(phoneNumber); // Cut spare delimiters
            Integer code = Integer.valueOf(phoneNumberModified.substring(0, 3)); // Extract 3 digit code
            String country = DIGIT_2_ISO.get(code);
            if (country == null) {
                code = Integer.valueOf(phoneNumberModified.substring(0, 2)); // Extract 2 digit code
                phoneNumberModified = phoneNumberModified.substring(2); // Now 'phoneNumberModified' has a plain number w/o code
                country = DIGIT_2_ISO.get(code);
            }
            else {
                phoneNumberModified = phoneNumberModified.substring(3); // Now 'phoneNumberModified' has a plain number w/o code
            }

            boolean isValid = false;
            if (country != null) {
                isValid = isNumValid(phoneNumberModified,
                                     RULE_START_PER_DIGIT.get(code),
                                     RULE_LENGTH_PER_DIGIT.get(code));
            }
            if (isValid) {
                result.validPhonesByCountry.computeIfAbsent(country, k -> new ArrayList<>()).add(phoneNumber);
            }
            else {
                result.invalidPhones.add(phoneNumber);
            }
        }

        return result;
    }

    public void printResults() {
        System.out.println("Valid phones:");
        System.out.println(result.validPhonesByCountry);
        System.out.println("\nInvalid phones:");
        System.out.println(result.invalidPhones);
    }

    private boolean isNumValid(String phone, Set<String> start, Set<Integer> len) {
        boolean isValid = false;
        for (String st : start) {
            isValid = phone.startsWith(st);
            if (isValid) {break;}
        }
        if (!isValid) {return isValid;}
        isValid = false;
        for (Integer l : len) {
            isValid = phone.length() == l;
            if (isValid) {
                return isValid;
            }
        }
        return isValid;
    }

    private String replaceSpareChars(String phoneNumberModified) {
        return phoneNumberModified.replaceAll("\\)", "")
                                  .replaceAll("\\(", "")
                                  .replaceAll(" ", "")
                                  .replaceAll("-", "")
                                  .replaceAll("\\+", "");
    }

}

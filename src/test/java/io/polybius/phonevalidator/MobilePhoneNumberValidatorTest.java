package io.polybius.phonevalidator;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MobilePhoneNumberValidatorTest {

    private final MobilePhoneNumberValidator validator = new MobilePhoneNumberValidator();

    @Test
    public void validate() {
        ValidationResultDto result = validator.validate(List.of("+37061234567"));
        assertEquals(List.of("+37061234567"), result.validPhonesByCountry.get("LT"));

        result = validator.validate(List.of("+37091234567"));
        assertEquals(List.of("+37091234567"), result.invalidPhones);

        result = validator.validate(List.of("+3706123456"));
        assertEquals(List.of("+3706123456"), result.invalidPhones);

    }

    @Test
    public void validateNew() {
        ValidationResultDto result = validator.validate(List.of("+324561234561"));
        assertEquals(List.of("+324561234561"), result.invalidPhones); // Extra digit, so phone is of 10 digits

        result = validator.validate(List.of("+3242612345"));
        assertEquals(List.of("+3242612345"),
                     result.invalidPhones); // Less than required digits, so phone is of 8 digits

        result = validator.validate(List.of("+32426123456"));
        assertEquals(List.of("+32426123456"),
                     result.invalidPhones); // 426, is not a number to start a valid phone for BE

        result = validator.validate(List.of("37161234567"));
        assertEquals(List.of("37161234567"),
                     result.invalidPhones); // 6, is not a number to start a valid phone for LT, should be 2


        result = validator.validate(List.of("+324(561)23456"));
        assertEquals(List.of("+324(561)23456"), result.validPhonesByCountry.get("BE"));

        result = validator.validate(List.of("37061234567"));
        assertEquals(List.of("37061234567"), result.validPhonesByCountry.get("LT"));

        result = validator.validate(List.of("37121234567"));
        assertEquals(List.of("37121234567"), result.validPhonesByCountry.get("LV"));

    }
}
